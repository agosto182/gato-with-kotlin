package com.ceti.ivanagosto.gato

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    var turn=1
    var cont=0
    var vs=0
    var inGame=0
    var buttons= arrayOf(arrayListOf<Button>(), arrayListOf<Button>(), arrayListOf<Button>())
    lateinit var dataBase:database
    lateinit var user:UserModel
    val timer = object : CountDownTimer(20000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            txtCounter.text = "Tiempo restante: " + millisUntilFinished / 1000
        }
        override fun onFinish() {
            inGame=0;
            showEndGame();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponents()
        dataBase=database(this)
        val id=intent.getStringExtra("user_id").toInt()
        Toast.makeText(this, "Usuario "+id.toString(), Toast.LENGTH_SHORT).show()
        user=dataBase.getUser(id)
        player1.setText(user.username.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflate= menuInflater
        inflate.inflate(R.menu.main_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.btnScores -> {
                val intent=Intent(this@MainActivity,scores::class.java)
                intent.putExtra("user_id",user.id.toString())
                startActivity(intent)
            }
            R.id.btnSesion -> {
                startActivity(Intent(this@MainActivity,Login::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun initComponents(){
        buttons[0]= arrayListOf(button11,button12,button13)
        buttons[1]= arrayListOf(button21,button22,button23)
        buttons[2]= arrayListOf(button31,button32,button33)

        for (arr in buttons){
            arr.let {
                for (btn in it){
                    btn.setOnClickListener { click(btn) }
                }
            }
        }
        btnRestart.setOnClickListener { start() }
        btnStartAlone.setOnClickListener {
            vs=0
            start()
        }
        btnStartVs.setOnClickListener {
            vs=1
            start()
        }
        btnEnd.setOnClickListener {
            inGame=0
            timer.cancel()
            btnStartVs.visibility=View.VISIBLE
            btnStartAlone.visibility=View.VISIBLE
            btnRestart.visibility= View.INVISIBLE
            btnEnd.visibility=View.INVISIBLE
        }

    }

    fun start(){
        cont=0
        inGame=1
        btnStartVs.visibility=View.INVISIBLE
        btnStartAlone.visibility=View.INVISIBLE
        btnRestart.visibility= View.VISIBLE
        btnEnd.visibility=View.VISIBLE
        if(vs==0) timer.start()
        for (arr in buttons){
            arr.let {
                for (btn in it){
                    btn.setText("-")
                }
            }
        }
    }

    fun click(btn:Button){
        if(btn.text.equals("-") && inGame==1 && cont!=9){
            if(vs==1){
                if(this.turn==1){
                    btn.setText("x")
                    this.turn=0
                    turnTxt.setText("Turno: Jugador2")
                }
                else{
                    btn.setText("o")
                    this.turn=1
                    turnTxt.setText("Turno: "+user.username.toString())
                }
                this.inc()
            }
            else{
                btn.setText("x")
                inc()
                checkWin()
                playPC()
                inc()
            }
            checkWin()
        }
        else{
            Toast.makeText(this, "Selecciona otro.", Toast.LENGTH_SHORT).show()
        }


    }

    fun inc(){
        this.cont++
        if(this.cont==9){
            showEndGame()
            timer.cancel()
            inGame=0
        }
    }

    fun checkWin(){
        val btn1=buttons[0]
        val btn2=buttons[1]
        val btn3=buttons[2]
        if(btn1[0].text.toString().equals(btn1[1].text.toString()) && btn1[1].text.toString().equals(btn1[2].text.toString()) && !btn1[0].text.toString().equals("-")){
            makeWinner(btn1[0])
        }
        else if(btn2[0].text.toString().equals(btn2[1].text.toString()) && btn2[1].text.toString().equals(btn2[2].text.toString()) && !btn2[0].text.toString().equals("-")){
            makeWinner(btn2[0])
        }
        else if(btn3[0].text.toString().equals(btn3[1].text.toString()) && btn3[1].text.toString().equals(btn3[2].text.toString()) && !btn3[0].text.toString().equals("-")){
            makeWinner(btn3[0])
        }
        else if(btn1[0].text.toString().equals(btn2[0].text.toString()) && btn1[0].text.toString().equals(btn3[0].text.toString()) && !btn1[0].text.toString().equals("-")){
            makeWinner(btn1[0])
        }
        else if(btn1[1].text.toString().equals(btn2[1].text.toString()) && btn1[1].text.toString().equals(btn3[1].text.toString()) && !btn1[1].text.toString().equals("-")){
            makeWinner(btn1[1])
        }
        else if(btn1[2].text.toString().equals(btn2[2].text.toString()) && btn1[2].text.toString().equals(btn3[2].text.toString()) && !btn1[2].text.toString().equals("-")){
            makeWinner(btn1[2])
        }
        else if(btn1[0].text.toString().equals(btn2[1].text.toString()) && btn1[0].text.toString().equals(btn3[2].text.toString()) && !btn1[0].text.toString().equals("-")){
            makeWinner(btn1[0])
        }
        else if(btn1[2].text.toString().equals(btn2[1].text.toString()) && btn1[2].text.toString().equals(btn3[0].text.toString()) && !btn1[2].text.toString().equals("-")){
            makeWinner(btn1[2])
        }

    }

    fun makeWinner(btn:Button){
        showEndGame()
        timer.cancel()
        if(vs==0 && btn.text.equals("x") && inGame==1){
            val scoreD=cont+getRandom()*getRandom()+getRandom()
            val data=ScoreModel(0,user.id,scoreD)
            dataBase.newScore(data)
            Toast.makeText(this, "Puntuacion "+ scoreD.toString() +".", Toast.LENGTH_SHORT).show()
            inGame=0
        }
    }


    fun playPC(){
        if(inGame==1 && cont!=9){
            val x=getRandom()
            val y=getRandom()
            println(x)
            println(y)
            var btn=buttons[x]
            if(btn[y].text.equals("-")){
                btn[y].text="o"
            }else{
                playPC()
            }
        }
    }

    fun getRandom(): Int {
        return Random().nextInt(3)
    }

    fun showEndGame()=Toast.makeText(this, "Juego termino.", Toast.LENGTH_SHORT).show()
}
