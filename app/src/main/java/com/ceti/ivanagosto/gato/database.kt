package com.ceti.ivanagosto.gato

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class database(context:Context): SQLiteOpenHelper(context,"gato",null,1) {
    val dbName="gato"
    private val dbUser="CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(30), name VARCHAR(30), email VARCHAR(30), password TEXT)"
    private val dbScore="CREATE TABLE scores(id INTEGER PRIMARY KEY AUTOINCREMENT, user_id int, score int)"
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(dbUser)
        db?.execSQL(dbScore)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE users")
        db?.execSQL("DROP TABLE scores")
        onCreate(db)
    }

    fun newUser(data: UserModel): Boolean {
        val db= writableDatabase
        val user=ContentValues()
        user.put("username",data.username.toString())
        user.put("name",data.name.toString())
        user.put("email",data.email.toString())
        user.put("password",data.password.toString())

        db.insert("users",null,user)

        return true
    }

    fun getUser(id: Int): UserModel {
        var user:UserModel
        val db=writableDatabase
        try {
            var cursor= db.rawQuery("SELECT * FROM users WHERE id="+id,null)
            cursor.moveToFirst()
            val id=cursor.getString(cursor.getColumnIndex("id")).toInt()
            val name=cursor.getString(cursor.getColumnIndex("name"))
            val username=cursor.getString(cursor.getColumnIndex("username"))
            val email=cursor.getString(cursor.getColumnIndex("email"))
            val password=cursor.getString(cursor.getColumnIndex("password"))

            user=UserModel(id,username,name,email,password)

        }catch (e:SQLiteException){
            db?.execSQL(dbUser)
            user=UserModel(-1,"","","","")
        }catch (e:Exception){
            user=UserModel(-1,"","","","")
        }
        return user
    }

    fun loginUser(username: String, password: String): Int {
        var id:Int
        val db=writableDatabase
        try {
            var cursor= db.rawQuery("SELECT * FROM users WHERE username LIKE '"+username+"' AND password LIKE '"+password+"'",null)
            cursor.moveToFirst()
            id=cursor.getString(cursor.getColumnIndex("id")).toInt()
        }catch (e:SQLiteException){
            db?.execSQL(dbUser)
            id=-1
        }catch (e:Exception){
            id=-1
        }
        return id
    }

    fun newScore(data: ScoreModel): Boolean {
        val db= writableDatabase
        val score=ContentValues()
        score.put("user_id",data.userId.toString())
        score.put("score",data.score.toString())

        db.insert("scores",null,score)

        return true
    }

    fun getPersonalScore(id: Int): ArrayList<ScoreModel> {
        var scores=ArrayList<ScoreModel>()
        val db= writableDatabase

        try {
            var cursor= db.rawQuery("SELECT * FROM scores WHERE user_id="+id+" ORDER BY score DESC LIMIT 6",null)
            cursor.moveToFirst()
            while(!cursor.isAfterLast){
                val id=cursor.getString(cursor.getColumnIndex("id")).toInt()
                val user_id=cursor.getString(cursor.getColumnIndex("user_id")).toInt()
                val scoreD=cursor.getString(cursor.getColumnIndex("score")).toInt()

                var score=ScoreModel(id,user_id,scoreD)
                scores.add(score)
                cursor.moveToNext()
            }

        }catch (e:SQLiteException){
            db?.execSQL(dbScore)
        }catch (e:Exception){

        }
        return scores
    }

}