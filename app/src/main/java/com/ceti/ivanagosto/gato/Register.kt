package com.ceti.ivanagosto.gato

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {
    lateinit var dataBase:database
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        dataBase= database(this)

        register.setOnClickListener { register() }
    }

    fun register(){
        val username= txtUser.text.toString()
        val name= txtName.text.toString()
        val email= txtEmail.text.toString()
        val password= txtPass.text.toString()

        val data=UserModel(0,username,name,email,password)
        dataBase.newUser(data)
        Toast.makeText(this, "Usuario "+ username +" registrado!", Toast.LENGTH_SHORT).show()
        startActivity(Intent(this@Register,Login::class.java))
    }

}
