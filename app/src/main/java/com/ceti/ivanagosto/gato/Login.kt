package com.ceti.ivanagosto.gato

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {
    lateinit var dataBase:database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        dataBase=database(this)

        tryLogin.setOnClickListener { login() }
        doRegister.setOnClickListener {
            startActivity(Intent(this@Login,Register::class.java))
        }
    }

    fun login(){
        val username=txtUser.text.toString()
        val password=txtPass.text.toString()
        val id=dataBase.loginUser(username,password)

        if(id!=-1){
            var intent=Intent(this@Login,MainActivity::class.java)
            intent.putExtra("user_id",id.toString())

            startActivity(intent)
        }
        else{
            Toast.makeText(this, "Datos incorrectos.", Toast.LENGTH_SHORT).show()
        }
    }




}
