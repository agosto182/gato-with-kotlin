package com.ceti.ivanagosto.gato

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_scores.*

class scores : AppCompatActivity() {
    lateinit var user:UserModel
    lateinit var scores:ArrayList<ScoreModel>
    lateinit var dataBase: database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scores)
        dataBase= database(this)
        val user_id= intent.getStringExtra("user_id").toInt()
        user= dataBase.getUser(user_id)
        scores= dataBase.getPersonalScore(user_id)

        updateView()
    }

    fun updateView(){
        txtUser.text= user.username
        txtEmail.text= user.email

        if(scores.size>0){
            if(scores.size>=1) txtPoints1.text=user.username+" - "+scores[0].score.toString()
            if(scores.size>=2) txtPoints2.text=user.username+" - "+scores[1].score.toString()
            if(scores.size>=3) txtPoints3.text=user.username+" - "+scores[2].score.toString()
            if(scores.size>=4) txtPoints4.text=user.username+" - "+scores[3].score.toString()
            if(scores.size>=5) txtPoints5.text=user.username+" - "+scores[4].score.toString()
        }
    }

}
